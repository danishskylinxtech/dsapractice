import UIKit

//MARK:  Algorithm 1
//Find the largest number among three numbers
var a = 100,b = 300, c = 50
//Check a is greater to b. if so then enter in condition
if (a > b)
{
    //Check a is geater to c, if so then a is greater otherwise, c is greater bcz c is greater to a and b.
    if (a > c)
    {
        print(a, "is greater")
    }
    else
    {
        print(c, "is greater")
    }
}
else
{
    if (b > c)
    {
        print(b, "is greater")
    }
    else
    {
        print(c, "is greater")
    }
}


//MARK:  Algorithm 2
//Find the smallest number among three numbers
var d = 10, e = 20, f = 7
if (d < e)
{
    if (d < f)
    {
        print(d, "is smaller")
    }
    else
    {
        print(f, "is smaller")
    }
}
else
{
    if (e < f)
    {
        print(e, "is smaller")
    }
    else
    {
        print(f, "is smaller")
    }
}

//MARK:  Algorithm 3
//Find the factorial of a number
var findFictnumber = 5
let downTo = 1
var fictorialResult = 1

while (findFictnumber > downTo)
{
    fictorialResult = findFictnumber * fictorialResult
    findFictnumber = findFictnumber - 1
}
// 5 * 4 * 3 * 2 * 1 = 120
print("Final fictorial is:-", fictorialResult)

//MARK:  Algorithm 4
//Check whether a number is prime or not
var primeNumber = 9
var value = 2
var primeFlag = false
while (value <= primeNumber / 2) // 2 <= 10 /2 ==> 2 <= 5  (Go inside)
{
    if (primeNumber % value == 0)
    {
        primeFlag = false
    }
    else
    {
        primeFlag = true
    }
    value = value + 1
}
if (primeFlag)
{
    print(primeNumber, "is a prime number")
}
else
{
    print(primeNumber, "is not a prime number")
}
//MARK:  Algorithm 5
func fibonacci(number: Int)
{
    var numberOne = 1
    var numberTwo = 0
    var FibArray : [Int] = []
    
    for _ in 0..<number
    {
        print("Fibonaci:", numberTwo)
        let final = numberOne + numberTwo
        numberOne = numberTwo
        numberTwo = final
        FibArray.append(numberTwo)
    }
    
    if FibArray.count != 0
    {
        for (index, _) in FibArray.enumerated()
        {
            if (index == number - 1)
            {
                let finalResult = FibArray[index - 1] + FibArray[index - 2]
                print(finalResult)
            }
        }
    }
}

//MARK:  Algorithm 6
//Make a array of numbers as first array. made another array from first array by leaving out the current index of first array and multiply all other index. for example if first array is [1,2,3,4] then second array should be [24,12,8,6]. on the first index of second array is a2[0] = a1[1] * a1[2] * a1[3]. Make it in programming with O(n square) means do not use array inside and array.

var arr1 = [5,4,7,9,1]
var arr2 : [Int] = []
var numb = 1
for item in arr1
{
    numb = item * numb
}
print(numb)

for item in arr1
{
    arr2.append(numb / item)
}
print(arr2)

//Stack
//MARK:  Algorithm 7
//Stack
struct Stack {
    var items : [String] = []
    
    //Pick the first element of stack
    func peek() -> String
    {
        guard let topElement = items.first else {fatalError("This stack is empty")}
        return topElement
    }
    
    //Remove First Element of stack
    mutating func pop() -> String
    {
        return items.removeFirst()
    }
    //Add item on First index
    mutating func push(element: String)
    {
         items.insert(element, at: 0)
    }
}
var stackObj = Stack()
stackObj.push(element: "Danish")
stackObj.push(element: "DanishMunir")
stackObj.push(element: "DanishDanish")
stackObj.push(element: "M.Danish Munir")
stackObj.push(element: "dskdk")
print(stackObj.items)
stackObj.pop()
print(stackObj.items)
print(stackObj.peek())

//MARK:  Algorithm 8
//Reverse a word stack algorithem
func reverseWord()
{
    let wordIs = "Danish"
    
   let wordArray = wordIs.map({ String($0) })
    var newWordIs = ""
    for i in wordArray.reversed()
    {
        newWordIs.append(i)
    }
    print(newWordIs)
}

print(reverseWord())


//0,1,1,2,3,5,8,13,21, 34
// Value Refrence, class refrence
//class C1
//{
//    var myString = "danish" 
//
//}
//
//struct S1
//{
//    var myClass = C1()
//    init(myClass: C1) {
//        self.myClass = myClass
//    }
//}
//
//let firstObj = C1()
//let secObj = S1(myClass: firstObj)
//let thirdObj = secObj
//
//firstObj.myString = "Dddd"
//secObj.myClass.myString = "Some"
//thirdObj.myClass.myString = "Other"
//
//print(firstObj.myString)
//print(secObj.myClass.myString)
//print(thirdObj.myClass.myString)

//class C1
//{
//    
//    var myClass = S1()
//    init(myClass: S1) {
//        self.myClass = myClass
//    }
//}
//
//struct S1
//{
//    var myString = "danish"
//    
//}
//
//var firstObj = S1()
//var secObj = C1(myClass: firstObj)
//var thirdObj = secObj
//
//firstObj.myString = "Dddd"
//secObj.myClass.myString = "Some"
//thirdObj.myClass.myString = "Other"
//
//print(firstObj.myString)
//print(secObj.myClass.myString)
//print(thirdObj.myClass.myString)
